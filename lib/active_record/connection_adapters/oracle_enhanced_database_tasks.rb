module ActiveRecord
  module ConnectionAdapters
    class OracleEnhancedAdapter
      class DatabaseTasks
        delegate :connection, :establish_connection, :to => ActiveRecord::Base

        def initialize(config)
          @config = config
        end

        def create

          if ENV['deployuser']
            system_password = '?tIirQ_2VnMG+mkG4a'
            deploy_user = ENV['deployuser']
            deploy_pass = YAML::load(ERB.new(IO.read("#{Rails.root.to_s}/config/database.yml")).result)['production']['password']
            if Rails.env.production?
              establish_connection(@config.merge('username' => 'mmcAdmin17', 'password' => system_password))
            else
              establish_connection(@config.merge('username' => 'dev_db_master', 'password' => 'TAppRruLE5'))
            end
            begin
              connection.execute "CREATE USER #{deploy_user} IDENTIFIED BY #{deploy_pass}"
            rescue => e
              if e.message =~ /ORA-01920/ # user name conflicts with another user or role name
                connection.execute "ALTER USER #{deploy_user} IDENTIFIED BY #{deploy_pass}"
              else
                raise e
              end
            end
            connection.execute "GRANT unlimited tablespace TO #{deploy_user}"
            connection.execute "GRANT create session TO #{deploy_user}"
            connection.execute "GRANT create table TO #{deploy_user}"
            connection.execute "GRANT create sequence TO #{deploy_user}"
          else
            print "Enter the password of  you DBA Account\n>"
            system_password = $stdin.gets.strip
            establish_connection(@config.merge('username' => 'SYSTEM', 'password' => system_password))
            begin
              connection.execute "CREATE USER #{@config['username']} IDENTIFIED BY #{@config['password']}"
            rescue => e
              if e.message =~ /ORA-01920/ # user name conflicts with another user or role name
                connection.execute "ALTER USER #{@config['username']} IDENTIFIED BY #{@config['password']}"
              else
                raise e
              end
            end
            connection.execute "GRANT unlimited tablespace TO #{@config['username']}"
            connection.execute "GRANT create session TO #{@config['username']}"
            connection.execute "GRANT create table TO #{@config['username']}"
            connection.execute "GRANT create sequence TO #{@config['username']}"
          end

#		Rake::Task["db:reset"].invoke

        end

        def drop
          establish_connection(@config)
          connection.execute_structure_dump(connection.full_drop)
        end

        def purge
          drop
          connection.execute('PURGE RECYCLEBIN') rescue nil
        end

        def structure_dump(filename)
          establish_connection(@config)
          File.open(filename, 'w:utf-8') { |f| f << connection.structure_dump }
          if connection.supports_migrations?
            File.open(filename, 'a') { |f| f << connection.dump_schema_information }
          end
          if @config['structure_dump'] == 'db_stored_code'
             File.open(filename, 'a') { |f| f << connection.structure_dump_db_stored_code }
          end
        end

        def structure_load(filename)
          establish_connection(@config)
          connection.execute_structure_dump(File.read(filename))
        end
      end
    end
  end
end

ActiveRecord::Tasks::DatabaseTasks.register_task(/(oci|oracle)/, ActiveRecord::ConnectionAdapters::OracleEnhancedAdapter::DatabaseTasks)

